var fs = require('fs'),
createCSVFile = require('csv-file-creator'),
json = JSON.parse(fs.readFileSync('../data/Accounts24AUG O-Z.json'));
const csv = require('csv-parser');

//var occRepArr = [];
var tmsRepArr = [];
var occRepArr = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
var iconv = require('iconv-lite');

var ouMapping = new Map([['025', 'Terex USA, LLC'], 
['401', 'Terex India  Private Limited'],['402', 'Terex India  Private Limited'],['733', 'Terex  Global GmbH'], ['735', 'Terex  Global GmbH'], ['719', 'Terex  Global GmbH'],['728', 'Terex  Global GmbH'], ['706', 'Terex  Global GmbH'], ['729', 'Terex  Global GmbH']]);

json.organization.forEach((organization, index) => {
	let billingData={};
	let data={};

	if(organization.active){

		if (organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId==organization.shippingAddress.repositoryId ){
			billingData=organization.billingAddress;
			billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
			billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
			billingData.brandShortName=organization.brandShortName;
			billingData.customerName=organization.name;
			billingData.occAccountId=organization.id;
			billingData.warehouseOrganizationCode=organization.warehouseOrganizationCode;
			billingData.repositoryId=organization.billingAddress.repositoryId;

			billingData.warehouseLocationId=organization.warehouseLocationId;
			billingData.tms_customerAccountSiteId=organization.billingAddress.tms_customerAccountSiteId;
			billingData.postalCode=organization.billingAddress.postalCode
			billingData.city=organization.billingAddress.city;
			billingData.country=organization.billingAddress.country;
			billingData.companyName=organization.billingAddress.companyName;
			billingData.phoneNumber=organization.billingAddress.phoneNumber;
			billingData.state=organization.billingAddress.state;
			billingData.address3=organization.billingAddress.address3;
			billingData.address2=organization.billingAddress.address2;
			billingData.address1=organization.billingAddress.address1;
			billingData.county=organization.billingAddress.county;
			billingData.tms_state=organization.billingAddress.tms_state;

			

			billingData.isDefaultBillto="P";
			billingData.isDefaultShipto="P";
			
			occRepArr.push([billingData.repositoryId,billingData.occAccountId,billingData.customerName,billingData.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,billingData.warehouseOrganizationCode,billingData.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.phoneNumber,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
			//occRepArr.push(billingData);

		}else if(organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId!=organization.shippingAddress.repositoryId ){
	
			data=organization.shippingAddress;
            data.tmsCustomerAccountId=organization.tmsCustomerAccountId;
            data.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
            data.brandShortName=organization.brandShortName;
            data.customerName=organization.name;
            data.occAccountId=organization.id;
            data.warehouseOrganizationCode=organization.warehouseOrganizationCode;
            data.repositoryId=organization.shippingAddress.repositoryId;

            data.warehouseLocationId=organization.warehouseLocationId;
            data.tms_customerAccountSiteId=organization.shippingAddress.tms_customerAccountSiteId;
            data.postalCode=organization.shippingAddress.postalCode
            data.city=organization.shippingAddress.city;
            data.country=organization.shippingAddress.country;
            data.companyName=organization.shippingAddress.companyName;
            data.phoneNumber=organization.shippingAddress.phoneNumber;
            data.state=organization.shippingAddress.state;
            data.address3=organization.shippingAddress.address3;
            data.address2=organization.shippingAddress.address2;
            data.address1=organization.shippingAddress.address1;
            data.county=organization.shippingAddress.county;
            data.tms_state=organization.shippingAddress.tms_state;


			data.isDefaultBillto="";
			data.isDefaultShipto="P";
			
			billingData=organization.billingAddress;
			billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
			billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
			billingData.brandShortName=organization.brandShortName;
			billingData.customerName=organization.name;
			billingData.occAccountId=organization.id;
			billingData.warehouseOrganizationCode=organization.warehouseOrganizationCode;
			billingData.repositoryId=organization.billingAddress.repositoryId;

			billingData.warehouseLocationId=organization.warehouseLocationId;
			billingData.tms_customerAccountSiteId=organization.billingAddress.tms_customerAccountSiteId;
			billingData.postalCode=organization.billingAddress.postalCode
			billingData.city=organization.billingAddress.city;
			billingData.country=organization.billingAddress.country;
			billingData.companyName=organization.billingAddress.companyName;
			billingData.phoneNumber=organization.billingAddress.phoneNumber;
			billingData.state=organization.billingAddress.state;
			billingData.address3=organization.billingAddress.address3;
			billingData.address2=organization.billingAddress.address2;
			billingData.address1=organization.billingAddress.address1;
			billingData.county=organization.billingAddress.county;
			billingData.tms_state=organization.billingAddress.tms_state;
			
			billingData.isDefaultBillto="P";
			billingData.isDefaultShipto="";

			occRepArr.push([data.repositoryId,data.occAccountId,data.customerName,data.brandShortName,data.tmsCustomerAccountId,data.tmsCustomerAccountNumber,data.warehouseOrganizationCode,data.warehouseLocationId,data.tms_customerAccountSiteId,data.postalCode,data.city,data.country,data.companyName,data.phoneNumber,data.state,data.address3,data.address2,data.address1,data.county,data.tms_state,data.isDefaultBillto,data.isDefaultShipto]);
			occRepArr.push([billingData.repositoryId,billingData.occAccountId,billingData.customerName,billingData.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,billingData.warehouseOrganizationCode,billingData.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.phoneNumber,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
			//occRepArr.push(data);
			//occRepArr.push(billingData);
		}
	}
});

createCSVFile("../output/Active Default Addresses of Fuchs Accounts in Production O-Z.csv",occRepArr);

// fs.createReadStream('../data/TMS Extract Defaults - 20 July.csv')
//     .pipe(iconv.decodeStream('windows-1252'))
//     .pipe(csv())
//     .on('data', function (data) {
//         try {
//             //console.log(data)
//             tmsRepArr.push(data);

//         }
//         catch (err) {
//             //error handler
//             console.log(err);
//         }
//     })
//     .on('end', function () {
//         getMissingSiteId(tmsRepArr, occRepArr);
//     });


// var getMissingSiteId = (tmsData, occData) => {
//     // let matchingTMSDefaultAddress = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	// let siteIDnotmatchingTMSDefaultAddress = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	// let notDefaultinCXC = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	// let noActiveDefaultAddressMatch = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	// let ounotMatching = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
	
// 	let fullymatchingCXCAddress = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	let matchingAddressWrongID = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	let matchingAddressButNotDefault = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	let addressmatchWrongIDNotDefault = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
// 	let notmatchingAddress = [['repositoryId','occAccountId','occAccountName','occBrand','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
	
// 	//console.log(tmsData.length);
//     for (let i = 0; i < tmsData.length; i++) {
//         let isMatchFound = false;
//         // let address1ComparePassed = false, address2ComparePassed = false, address3ComparePassed = false,cityComparePassed = false, postalCodeComparePassed = false,
//         // stateComparePassed = false, countryComparePassed = false,billToComparePassed= false,shipToComparePassed=false;

//         for (let j = 0; j < occData.length; j++) {
// 			//console.log(occData.length);
//            if (checkForMissingId(tmsRepArr[i], occRepArr[j])) {

// 				//console.log('Comapring');
			    
// 				if (tmsData[i].CUSTOMERACCOUNTSITEID==occData[j].tms_customerAccountSiteId)  {
					
// 					let address1ComparePassed = false, address2ComparePassed = false, address3ComparePassed = false,cityComparePassed = false, postalCodeComparePassed = false,
//         			stateComparePassed = false, countryComparePassed = false,billToComparePassed= false,shipToComparePassed=false;
					
// 					let occAddress1 = occData[j].address1? occData[j].address1.toUpperCase(): "";
// 					if(tmsData[i].ADDRESS1.toUpperCase()==occAddress1 ) {
// 						address1ComparePassed = true;
// 					}
					
// 					let occAddress2 = occData[j].address2? occData[j].address2.toUpperCase(): ""; 
// 					if(tmsData[i].ADDRESS2.toUpperCase()== occAddress2  ) {
// 						address2ComparePassed = true;
// 					} 
					
// 					let occAddress3 = occData[j].address3? occData[j].address3.toUpperCase(): ""; 
// 					if(tmsData[i].ADDRESS3.toUpperCase()== occAddress3  ) {
// 						address3ComparePassed = true;
// 					} 


// 					let occCity = occData[j].city ? occData[j].city.toUpperCase(): "";

// 					if(tmsData[i].CITY.toUpperCase()==occCity) {
// 						cityComparePassed = true;
// 					}

// 					let occPostalCode = occData[j].postalCode? occData[j].postalCode : ""; 
// 					if(tmsData[i].POSTALCODE.toUpperCase()==occData[j].postalCode) {
// 						postalCodeComparePassed = true;
						
// 					}
// 					let occState = occData[j].tms_state ? occData[j].tms_state.toUpperCase() : "";

// 					if(tmsData[i].STATE.toUpperCase()==occState) {
// 						stateComparePassed = true;
// 					}

// 					let occCountry = occData[j].country ? occData[j].country.toUpperCase() : ""; 
// 					if(tmsData[i].COUNTRY.toUpperCase()==occCountry) {
// 						countryComparePassed = true;
// 					}
					
// 					let occbillTo = occData[j].isDefaultBillto ? occData[j].isDefaultBillto.toUpperCase() : ""; 
// 					if(tmsData[i].CUSTACCTBILLTOFLAG.toUpperCase()==occbillTo || (tmsData[i].CUSTACCTBILLTOFLAG.toUpperCase()=='Y' && occbillTo=="" )){
// 						billToComparePassed=true;
						
// 					}
// 					let occshipTo = occData[j].isDefaultShipto ? occData[j].isDefaultShipto.toUpperCase() : ""; 
// 					if(tmsData[i].CUSTACCTSHIPTOFLAG.toUpperCase()==occshipTo || (tmsData[i].CUSTACCTSHIPTOFLAG.toUpperCase()=="Y" && occshipTo=="" )){
// 						shipToComparePassed=true;
						
// 					}
// 					if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && billToComparePassed && shipToComparePassed) {
// 						isMatchFound=true;
// 						console.log('Full Match Found');
// 						//matchingTMSDefaultAddress.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,tmsData[i].CUSTOMERACCOUNTNUMBER,tmsData[i].CUSTOMERNAME,tmsData[i].ADDRESS1,tmsData[i].ADDRESS2,tmsData[i].Address3,tmsData[i].CITY,tmsData[i].STATE,tmsData[i].COUNTRY,tmsData[i].POSTALCODE,tmsData[i].COUNTY,tmsData[i].OCC_REPOSITORY_ID,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 						fullymatchingCXCAddress.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,occData[j].tms_customerAccountSiteId,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,occData[j].isDefaultBillto,occData[j].isDefaultShipto]);
// 					}
// 					else if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && !billToComparePassed && shipToComparePassed){
// 						isMatchFound=true;
// 						console.log('Not Default');
// 						matchingAddressButNotDefault.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 					}
// 					else if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && billToComparePassed && !shipToComparePassed){
// 						isMatchFound=true;
// 						console.log('Not Default');
// 						matchingAddressButNotDefault.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 					}

// 				}
// 				else if(tmsData[i].CUSTOMERACCOUNTSITEID!=occData[j].tms_customerAccountSiteId)  {
					
// 					let address1ComparePassed = false, address2ComparePassed = false, address3ComparePassed = false,cityComparePassed = false, postalCodeComparePassed = false,
//         			stateComparePassed = false, countryComparePassed = false,billToComparePassed= false,shipToComparePassed=false;
					
// 					let occAddress1 = occData[j].address1? occData[j].address1.toUpperCase(): "";
// 					if(tmsData[i].ADDRESS1.toUpperCase()==occAddress1 ) {
// 						address1ComparePassed = true;
// 					}
					
// 					let occAddress2 = occData[j].address2? occData[j].address2.toUpperCase(): ""; 
// 					if(tmsData[i].ADDRESS2.toUpperCase()== occAddress2  ) {
// 						address2ComparePassed = true;
// 					} 
					
// 					let occAddress3 = occData[j].address3? occData[j].address3.toUpperCase(): ""; 
// 					if(tmsData[i].ADDRESS3.toUpperCase()== occAddress3  ) {
// 						address3ComparePassed = true;
// 					} 


// 					let occCity = occData[j].city ? occData[j].city.toUpperCase(): "";

// 					if(tmsData[i].CITY.toUpperCase()==occCity) {
// 						cityComparePassed = true;
// 					}

// 					let occPostalCode = occData[j].postalCode? occData[j].postalCode : ""; 
// 					if(tmsData[i].POSTALCODE.toUpperCase()==occData[j].postalCode) {
// 						postalCodeComparePassed = true;
						
// 					}
// 					let occState = occData[j].tms_state ? occData[j].tms_state.toUpperCase() : "";

// 					if(tmsData[i].STATE.toUpperCase()==occState) {
// 						stateComparePassed = true;
// 					}

// 					let occCountry = occData[j].country ? occData[j].country.toUpperCase() : ""; 
// 					if(tmsData[i].COUNTRY.toUpperCase()==occCountry) {
// 						countryComparePassed = true;
// 					}
					
// 					let occbillTo = occData[j].isDefaultBillto ? occData[j].isDefaultBillto.toUpperCase() : ""; 
// 					if(tmsData[i].CUSTACCTBILLTOFLAG.toUpperCase()==occbillTo || (tmsData[i].CUSTACCTBILLTOFLAG.toUpperCase()=='Y' && occbillTo=="" )){
// 						billToComparePassed=true;
						
// 					}
// 					let occshipTo = occData[j].isDefaultShipto ? occData[j].isDefaultShipto.toUpperCase() : ""; 
// 					if(tmsData[i].CUSTACCTSHIPTOFLAG.toUpperCase()==occshipTo || (tmsData[i].CUSTACCTSHIPTOFLAG.toUpperCase()=="Y" && occshipTo=="" )){
// 						shipToComparePassed=true;
						
// 					}
// 					if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && billToComparePassed && shipToComparePassed) {
// 						isMatchFound=true;
// 						console.log('SiteID not Matching');
// 						//siteIDnotmatchingTMSDefaultAddress.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,tmsData[i].CUSTOMERACCOUNTNUMBER,tmsData[i].CUSTOMERNAME,tmsData[i].ADDRESS1,tmsData[i].ADDRESS2,tmsData[i].Address3,tmsData[i].CITY,tmsData[i].STATE,tmsData[i].COUNTRY,tmsData[i].POSTALCODE,tmsData[i].COUNTY,tmsData[i].OCC_REPOSITORY_ID,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 						matchingAddressWrongID.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,occData[j].isDefaultBillto,occData[j].isDefaultShipto]);
// 					}
// 					else if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && !billToComparePassed && shipToComparePassed){
// 						isMatchFound=true;
// 						console.log('Not Default Site ID Wrong');
// 						addressmatchWrongIDNotDefault.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 					}
// 					else if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && billToComparePassed && !shipToComparePassed){
// 						isMatchFound=true;
// 						console.log('Not Default Site ID Wrong');
// 						addressmatchWrongIDNotDefault.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 					}
// 				}
// 				else if(!isMatchFound){
// 					console.log('NoMatch');
// 					//noActiveDefaultAddressMatch.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,tmsData[i].CUSTOMERACCOUNTNUMBER,tmsData[i].CUSTOMERNAME,tmsData[i].ADDRESS1,tmsData[i].ADDRESS2,tmsData[i].Address3,tmsData[i].CITY,tmsData[i].STATE,tmsData[i].COUNTRY,tmsData[i].POSTALCODE,tmsData[i].COUNTY,tmsData[i].OCC_REPOSITORY_ID,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
// 					notmatchingAddress.push([occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,occData[j].brandShortName,occData[j].tmsCustomerAccountId,occData[j].tmsCustomerAccountNumber,occData[j].warehouseOrganizationCode,occData[j].warehouseLocationId,occData[j].tms_customerAccountSiteId,occData[j].postalCode,occData[j].city,occData[j].country,occData[j].companyName,occData[j].phoneNumber,occData[j].state,occData[j].address3,occData[j].address2,occData[j].address1,occData[j].county,occData[j].tms_state,occData[j].isDefaultBillto,occData[j].isDefaultShipto]);
// 				}
// 			}
// 		}
		
// 	}

// //    createCSVFile("../output/Active Matching TMS Address (All Details Matching).csv", matchingTMSDefaultAddress);
// //    createCSVFile("../output/Active Matching TMS Address (SITE ID not Matching).csv", siteIDnotmatchingTMSDefaultAddress);
// //    createCSVFile("../output/Active Matching Addresses but not Default.csv", notDefaultinCXC);
// //    createCSVFile("../output/Default in TMS But Not Matching in CXC.csv", noActiveDefaultAddressMatch);

//       createCSVFile("../output/Address in CXC Fully Matching.csv", fullymatchingCXCAddress);
// 	  createCSVFile("../output/Address in CXC Matching with Wrong SiteID.csv", matchingAddressWrongID);
// 	  createCSVFile("../output/Address in CXC Matching but not Default.csv", matchingAddressButNotDefault);
// 	  createCSVFile("../output/Address in CXC Wrong Site ID and not Default.csv", addressmatchWrongIDNotDefault);
// 	  createCSVFile("../output/Address in CXC Not Matching with TMS.csv", notmatchingAddress);

// }

// function checkForMissingId(tmsData, occData) {
//     let warehouseOrganizationCode = occData.warehouseOrganizationCode;
   

//     if(tmsData.CUSTOMERACCOUNTID == occData.tmsCustomerAccountId  && ouMapping.has(warehouseOrganizationCode) && tmsData.OPERATINGUNIT==ouMapping.get(warehouseOrganizationCode)&& tmsData.CUSTOMERACCOUNTNUMBER==occData.tmsCustomerAccountNumber) {
//         return true;   
//     }

//   return false;        
// }


