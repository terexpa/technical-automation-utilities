var fs = require('fs'),
    createCSVFile = require('csv-file-creator'),
    json = JSON.parse(fs.readFileSync('../data/Accounts.json'));
const csv = require('csv-parser');
const moment = require('moment');

var occRepArr = [];
var tmsRepArr = [];
var iconv = require('iconv-lite');

var xl = require('excel4node');
let rowCounter = 2;                

// Create a new instance of a Workbook class
var wb = new xl.Workbook();

// Add Worksheets to the workbook
var ws = wb.addWorksheet('Sheet 1');
var ws2 = wb.addWorksheet('Sheet 2');

// Create a reusable style
var style = wb.createStyle({
font: {
    color: '#FF0800',
    size: 12,
}
});
initializeHeaders(wb);

var ouMapping = new Map([['025', 'Terex USA, LLC'], 
['401', 'Terex India  Private Limited'], ['735', 'Terex  Global GmbH'], ['719', 'Terex  Global GmbH'],['728', 'Terex  Global GmbH'], ['706', 'Terex  Global GmbH'], ['729', 'Terex  Global GmbH']]);



json.organization.forEach((organization, index) => {
	let billingData={};
	let data={}
			
    if (organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId==organization.shippingAddress.repositoryId ){
		billingData=organization.billingAddress;
		billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		billingData.brandShortName=organization.brandShortName;
		billingData.customerName=organization.name;
		billingData.occAccountId=organization.id;
		billingData.warehouseOrganizationCode=organization.warehouseOrganizationCode;
		billingData.isDefaultBillto="P";
		billingData.isDefaultShipto="P";
		//console.log("billTO");
		
		occRepArr.push(billingData);
		//accData.push([billingData.repositoryId,organization.id,organization.name,organization.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
		
    }else if(organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId!=organization.shippingAddress.repositoryId ){
		//console.log("shipTO");
		data=organization.shippingAddress;
		data.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		data.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		data.brandShortName=organization.brandShortName;
		data.customerName=organization.name;
		data.occAccountId=organization.id;
		data.warehouseOrganizationCode=organization.warehouseOrganizationCode;
		data.isDefaultBillto="";
		data.isDefaultShipto="P";
		
		billingData=organization.billingAddress;
		billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		billingData.brandShortName=organization.brandShortName;
		billingData.customerName=organization.name;
		billingData.occAccountId=organization.id;
		billingData.warehouseOrganizationCode=organization.warehouseOrganizationCode;
		billingData.isDefaultBillto="P";
		billingData.isDefaultShipto="";
		
		occRepArr.push(data);
		occRepArr.push(billingData);
		
		//accData.push([data.repositoryId,organization.id,organization.name,organization.brandShortName,data.tmsCustomerAccountId,data.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,data.tms_customerAccountSiteId,data.postalCode,data.city,data.country,data.companyName,data.state,data.address3,data.address2,data.address1,data.county,data.tms_state,data.isDefaultBillto,data.isDefaultShipto]);
		//accData.push([billingData.repositoryId,organization.id,organization.name,organization.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
		
	}else if(!organization.billingAddress){
		billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		billingData.brandShortName=organization.brandShortName;
		billingData.customerName=organization.name;
		billingData.occAccountId=organization.id;
		billingData.warehouseOrganizationCode=organization.warehouseOrganizationCode;
		billingData.isDefaultBillto="";
		billingData.isDefaultShipto="";
		occRepArr.push(billingData);
		//accData.push(["",organization.id,organization.name,organization.brandShortName,"","",organization.warehouseOrganizationCode,organization.warehouseLocationId,"","","","","","","","","","","",billingData.isDefaultBillto,billingData.isDefaultShipto]);
	
	}else if(!organization.shippingAddress){
		data.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		data.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		data.brandShortName=organization.brandShortName;
		data.customerName=organization.name;
		data.occAccountId=organization.id;
		data.warehouseOrganizationCode=organization.warehouseOrganizationCode;
		data.isDefaultBillto="";
		data.isDefaultShipto="";
		occRepArr.push(data);
		//accData.push(["",organization.id,organization.name,organization.brandShortName,"","",organization.warehouseOrganizationCode,organization.warehouseLocationId,"","","","","","","","","","","",data.isDefaultBillto,data.isDefaultShipto]);
	}
});

    fs.createReadStream('../data/TMS_Address_Extract.csv')
    .pipe(iconv.decodeStream('windows-1252'))
    .pipe(csv())
    .on('data', function (data) {
        try {
            //console.log(data)
            tmsRepArr.push(data);

        }
        catch (err) {
            //error handler
            console.log(err);
        }
    })
    .on('end', function () {
        getMissingSiteId(tmsRepArr, occRepArr);
    });


var getMissingSiteId = (tmsData, occData) => {
    let missingTMSAccountIds = [['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERACCOUNTNUMBER','CUSTOMERNAME','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','COUNTY','OCCREPOSITORYID']];
	let matchingSiteIds=[['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','CUSTOMERNAME','OCCACCOUNTID','OCCREPOSITORYID']];
	let updateSiteIds=[['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','OCCREPOSITORYID','OCCACCOUNTID','CUSTOMERNAME']];
	let siteIds=[['TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTNUMBER','CUSTOMERACCOUNTSITEID','CUSTOMERNAME','OCCACCOUNTID','ADDRESS1','ADDRESS2','ADDRESS3','CITY','STATE','COUNTRY','POSTAL_CODE','PHONENUMBER','COUNTY','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
	

    for (let i = 0; i < tmsData.length; i++) {
        let isMatchFound = false;
        let address1ComparePassed = false, address2ComparePassed = false, address3ComparePassed = false,cityComparePassed = false, postalCodeComparePassed = false,
        stateComparePassed = false, countryComparePassed = false,billToComparePassed= false,shipToComparePassed=false;

        for (let j = 0; j < occData.length; j++) {
           if (checkForMissingId(tmsRepArr[i], occRepArr[j])) {
			    
				if(tmsData[i].CUSTOMERACCOUNTSITEID==occData[j].tms_customerAccountSiteId){
					console.log("site id matches");
					isMatchFound = true;
					matchingSiteIds.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].customerName,occData[j].occAccountId,occData[j].repositoryId]);
					break;
				}else if (tmsData[i].CUSTOMERACCOUNTSITEID!=occData[j].tms_customerAccountSiteId && tmsData[i].CUSTACCTBILLTOFLAG==occData[j].isDefaultBillto && tmsData[i].CUSTACCTSHIPTOFLAG==occData[j].isDefaultShipto)  {
					console.log("not matches");
					let occAddress1 = occData[j].address1? occData[j].address1.toUpperCase(): "";
					if(tmsData[i].ADDRESS1.toUpperCase()==occAddress1 ) {
						address1ComparePassed = true;
					}
					
					let occAddress2 = occData[j].address2? occData[j].address2.toUpperCase(): ""; 
					if(tmsData[i].ADDRESS2.toUpperCase()== occAddress2  ) {
						address2ComparePassed = true;
					} 
					
					let occAddress3 = occData[j].address3? occData[j].address3.toUpperCase(): ""; 
					if(tmsData[i].ADDRESS3.toUpperCase()== occAddress3  ) {
						address3ComparePassed = true;
					} 


					let occCity = occData[j].city ? occData[j].city.toUpperCase(): "";

					if(tmsData[i].CITY.toUpperCase()==occCity) {
						cityComparePassed = true;
					}

					let occPostalCode = occData[j].postalCode? occData[j].postalCode : ""; 
					if(tmsData[i].POSTALCODE.toUpperCase()==occData[j].postalCode) {
						postalCodeComparePassed = true;
						
					}
					let occState = occData[j].tms_state ? occData[j].tms_state.toUpperCase() : "";

					if(tmsData[i].STATE.toUpperCase()==occState) {
						stateComparePassed = true;
					}

					let occCountry = occData[j].country ? occData[j].country.toUpperCase() : ""; 
					if(tmsData[i].COUNTRY.toUpperCase()==occCountry) {
						countryComparePassed = true;
					}
					
					let occbillTo = occData[j].isDefaultBillto ? occData[j].isDefaultBillto.toUpperCase() : ""; 
					if(tmsData[i].CUSTACCTBILLTOFLAG.toUpperCase()==occbillTo){
						billToComparePassed=true;
						
					}
					let occshipTo = occData[j].isDefaultShipto ? occData[j].isDefaultShipto.toUpperCase() : ""; 
					if(tmsData[i].CUSTACCTSHIPTOFLAG.toUpperCase()==occshipTo){
						shipToComparePassed=true;
						
					}
					if(address1ComparePassed && address2ComparePassed && cityComparePassed && address3ComparePassed && countryComparePassed && postalCodeComparePassed && billToComparePassed && shipToComparePassed) {
						ws.cell(rowCounter, 1)
						.string(occData[j].tms_customerAccountSiteId);
						
						ws.cell(rowCounter, 2)
						.string(occData[j].address1);
						
						ws.cell(rowCounter, 3)
						.string(occData[j].address2);
						
						ws.cell(rowCounter, 4)
						.string(occData[j].address3);
						
						ws.cell(rowCounter, 5)
						.string(occData[j].postalCode);
						
						ws.cell(rowCounter, 6)
						.string(occData[j].city);
						
						ws.cell(rowCounter,7)
						.string(occData[j].tms_state);
						
						ws.cell(rowCounter,8)
						.string(occData[j].state);
						
						ws.cell(rowCounter, 9).string(occData[j].country);
						ws.cell(rowCounter, 10).string(occData[j].county);
						ws.cell(rowCounter, 11).string(occData[j].tmsCustomerAccountId);
						ws.cell(rowCounter, 12).string(occData[j].tmsCustomerAccountNumber);
						ws.cell(rowCounter, 13).string(occData[j].repositoryId);
						ws.cell(rowCounter, 14).string(occData[j].customerName);
						ws.cell(rowCounter, 15).string(occData[j].occAccountId);
						
					   ws.cell(rowCounter,16)
						.string(tmsData[i].CUSTOMERACCOUNTSITEID);
															 
					   ws.cell(rowCounter,17)
						.string(tmsData[i].ADDRESS1);

						
						
						ws.cell(rowCounter, 18)
						.string(tmsData[i].ADDRESS2);
						ws.cell(rowCounter, 19)
						.string(tmsData[i].ADDRESS3);
						
						
						
						ws.cell(rowCounter, 20)
						.string(tmsData[i].POSTAL_CODE);

						
						
				
						ws.cell(rowCounter, 21)
						.string(tmsData[i].CITY);

						
						
						ws.cell(rowCounter, 22)
						.string(tmsData[i].STATE);

						
					
						
						ws.cell(rowCounter, 23)
						.string(tmsData[i].COUNTRY);

						
						 ws.cell(rowCounter, 24)
						 .string(tmsData[i].COUNTY);
						 
						 ws.cell(rowCounter, 25)
						 .string(tmsData[i].CUSTOMERACCOUNTID);

						 ws.cell(rowCounter,26)
						 .string(tmsData[i].CUSTOMERACCOUNTNUMBER);
						 ws.cell(rowCounter, 27)
						.string(tmsData[i].CUSTOMERNAME);
						 
						
						
						ws.cell(rowCounter, 28)
						.string(tmsData[i].OPERATINGUNIT);

						rowCounter++;
						isMatchFound=true;
						updateSiteIds.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,occData[j].repositoryId,occData[j].occAccountId,occData[j].customerName,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);
						break;
						
					}
						siteIds.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTNUMBER,tmsData[i].CUSTOMERACCOUNTSITEID,tmsData[i].CUSTOMERNAME,occData[j].occAccountId,tmsData[i].ADDRESS1,tmsData[i].ADDRESS2,tmsData[i].Address3,tmsData[i].CITY,tmsData[i].STATE,tmsData[i].COUNTRY,tmsData[i].POSTALCODE,tmsData[i].PHONENUMBER,tmsData[i].COUNTY,tmsData[i].CUSTACCTBILLTOFLAG,tmsData[i].CUSTACCTSHIPTOFLAG]);

		
				}
		   
			
			}
		}
		if(!isMatchFound) {
             missingTMSAccountIds.push([tmsData[i].CUSTOMERACCOUNTID,tmsData[i].CUSTOMERACCOUNTSITEID,tmsData[i].CUSTOMERACCOUNTNUMBER,tmsData[i].CUSTOMERNAME,tmsData[i].ADDRESS1,tmsData[i].ADDRESS2,tmsData[i].Address3,tmsData[i].CITY,tmsData[i].STATE,tmsData[i].COUNTRY,tmsData[i].POSTALCODE,tmsData[i].COUNTY,tmsData[i].OCC_REPOSITORY_ID]);
        }
		
	}
	//wb.write('../output/updateAddress'+moment().format("YYYY-MM-DD-HHmm")+'.xlsx');

   console.log(missingTMSAccountIds.length);
   createCSVFile("../output/NoSiteIdInOCC.csv", siteIds);
   //createCSVFile("../output/MatchingAddressInOCC.csv", matchingSiteIds);
   //createCSVFile("../output/AddressesNotInOCC.csv", missingTMSAccountIds);
   //createCSVFile("../output/UpdateSiteId.csv", updateSiteIds);
   
}
function initializeHeaders(wb){
    ws.cell(1, 1)
    .string("OCC site id ");
    ws.cell(1, 2)
    .string("OCC Address1");
    ws.cell(1, 3)
    .string("OCC Address2");
	ws.cell(1, 4)
    .string("OCC Address3");
    
    ws.cell(1, 5)
    .string("OCC Postal code");
	ws.cell(1, 6)
    .string("OCC City");
    ws.cell(1, 7)
    .string("OCC tms_State");
    ws.cell(1, 8)
    .string("OCC State");
    ws.cell(1, 9)
    .string("OCC Country");
	ws.cell(1, 10)
    .string("OCC County");
	ws.cell(1, 11)
    .string("OCC tmscustomerAccountId");
	ws.cell(1, 12)
    .string("OCC tmscustomerAccountNumber");
	ws.cell(1, 13)
    .string("OCC repositoryId");
	ws.cell(1, 14)
    .string("OCC CustomerName");
	ws.cell(1, 15)
    .string("OCC AccountId");
	



    ws.cell(1, 16)
    .string("TMS CUSTOMERACCOUNTSITEID");
    ws.cell(1, 17)
    .string("TMS Address1");
    ws.cell(1, 18)
    .string("TMS Address2");
	ws.cell(1, 19)
    .string("TMS Address3");
	ws.cell(1, 20)
    .string("TMS Postal code");
    ws.cell(1, 21)
    .string("TMS City");
    
    ws.cell(1, 22)
    .string("TMS State");
    ws.cell(1, 23)
    .string("TMS Country");
     ws.cell(1, 24)
     .string("TMS County");
    ws.cell(1, 25)
    .string("TMS CUSTOMERACCOUNTID");

    ws.cell(1, 26)
    .string("TMS CUSTOMERACCOUNTNUMBER");

    ws.cell(1, 27)
    .string("TMS CUSTOMERNAME");

    
	
	ws.cell(1,28)
	.string("TMS OU Name");

}
function checkForMissingId(tmsData, occData) {
    let warehouseOrganizationCode = occData.warehouseOrganizationCode;
   

    if(tmsData.CUSTOMERACCOUNTID == occData.tmsCustomerAccountId  && ouMapping.has(warehouseOrganizationCode) && tmsData.OPERATINGUNIT==ouMapping.get(warehouseOrganizationCode)&& tmsData.CUSTOMERACCOUNTNUMBER==occData.tmsCustomerAccountNumber) {
        return true;   
    }

  return false;        
 }



