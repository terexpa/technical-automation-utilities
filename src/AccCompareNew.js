var fs = require('fs'),
         createCSVFile = require('csv-file-creator'),
         fileName = '../data/Accounts29JulyAfterCorrection.json';
         allAcc = JSON.parse(fs.readFileSync(fileName));
const csv = require('csv-parser');
var subRepArr = [['ParentOrgId','SubOrgId','Name','brandShortName']];
var priRepArr = []; 
//var ouMapping = new Map([['025', 'Terex USA, LLC'], 
//['401', 'Terex India  Private Limited'],['402', 'Terex India  Private Limited'], ['735', 'Terex  Global GmbH'], ['719', 'Terex  Global GmbH'],['728', 'Terex  Global GmbH'], ['706', 'Terex  Global GmbH'], ['729', 'Terex  Global GmbH'],['733', 'Terex  Global GmbH']]);   

allAcc.organization.forEach(function(organization){
    try{
    		
        subRepArr.push([organization.parentOrganization.id,organization.id,organization.name,organization.brandShortName]);
        //console.log(subRepArr);
						
            
        }catch(err){
            console.log(organization.id);
            console.log(err);

        }
    

});
fs.createReadStream('../data/PurePrincipalAccountwithPaymentMethod.csv')
.pipe(csv())
.on('data', function(data){
    try {
        //console.log('repository id:'+data.OCC_REPOSITORY_ID);
        priRepArr.push([data]);
        
    }
    catch(err) {
        //error handler
    }
})
.on('end',function(){
    //some final operation
    getMissingRepoId(priRepArr, subRepArr);
	
	
	
}); 

var getMissingRepoId = (priRepArr, subRepArr) => {
     console.log(priRepArr.length, subRepArr.length);
     let matchingRepoIds = [['ParentOrgId','SubOrgId','Name','PaymentMethod','brandShortName']];
	 
        
     for(let i = 1;  i < priRepArr.length; i++) {
         let isMatchFound = false;
		 
    
        for(let j = 1; j < subRepArr.length; j++) {
			
             if(priRepArr[i][0].id==subRepArr[j][0]){
                isMatchFound = true;
				console.log(priRepArr[i][0].paymentMethods);
				matchingRepoIds.push([subRepArr[j][0],subRepArr[j][1],subRepArr[j][2],priRepArr[i][0].paymentMethods,priRepArr[i][0].brandShortName]);
                 break;
             }
        }
        
     }

     //console.log(missingRepoIds);
	 createCSVFile("../output/AccMatchStageUpdatePayNewScript.csv",matchingRepoIds);
	 
	 console.log("file generated successfully");
	 return matchingRepoIds;
	
 }