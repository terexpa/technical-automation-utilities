const Client = require('node-rest-client').Client;
const fs = require('fs');
//Needed to parse response when mimeTypes is not set :util.inspect(object)
//var util = require('util');
const csv = require('csv-parser');
let profileIds = [];
//Setup logs
const log = require('simple-node-logger').createSimpleLogger('../log/address-update2.log');
log.setLevel('all');

// customize response to avoid circular structure
 var options = { 
    mimetypes:{
        json:["application/json","application/json; charset=utf-8"]
    }  
 };
 
var client = new Client(options);

function createRequestdata(data) {
    let reqData = {};
	let addressData = {
	tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID
	};
	reqData={
				"address":addressData
				
			}
    return reqData;
}


function readUpdateCSVFile() {
    fs.createReadStream('../data/abc.csv')
    .pipe(csv())
    .on('data', function(data){
        try {
            //  createRequestdata(data);
            profileIds.push(data);
        }
        catch(err) {
            log.error('Error while reading CSV', err);
        }
    })
    .on('end',function(){
		var adminAuth='';
		var loginReqData =	{
			grant_type: "password",
			username: "unnati.barot@lntinfotech.com",
			password: "Pramukh100",
			totp_code: "218419"
		};
		let args={
				data:loginReqData,
				headers:{"Content-Type": "application/x-www-form-urlencoded"}
		};
		client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/mfalogin", args, function(data, response){
			console.log(JSON.parse(data));
			console.log("status" +response.statusCode);
			adminAuth=JSON.parse(data);
			console.log("accessToken:" +adminAuth.access_token);
			if(adminAuth.access_token!=""){
			var x = 0;

			var loopArray = function(arr) {
				// call itself
				customAlert(arr[x],arr.length,function(){
					// set x to next item
					x++;
					// any more items in array?
					if(x < arr.length) {
						loopArray(arr);   
					}
				}); 
			}	
			loopArray(profileIds);	
				function customAlert(profileArray,arrLength,outercallback) {
				let reqData = createRequestdata(profileArray);
							var auth_token = adminAuth.access_token;
							let args = {
								data: reqData,
								requestConfig: {
									timeout: 5000 //request timeout in milliseconds
									//noDelay: true, //Enable/disable the Nagle algorithm
									//keepAlive: true, //Enable/disable keep-alive functionalityidle socket.
									//keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent
								},
								responseConfig: {
									timeout: 5000 //response timeout
								},
							   path:{"id":profileArray.OCCACCOUNTID,"addressId":profileArray.OCCREPOSITORYID},
								headers: { "Content-Type": "application/json","Authorization":'Bearer'+' '+auth_token}
							};
							
							log.debug("Request Data for org:"+ args.path.id+ " repositoryId:"+args.path.addressId,reqData);
							process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

							var req = client.put("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/organizations/${id}/secondaryAddresses/${addressId}", args, function(data, response) {
								
								//console.log(JSON.parse(data));
								if(response.statusCode!="401"){
									outercallback();
								}
								log.debug("Response for Org:"+response.statusCode,JSON.parse(data));
								if(x==arrLength-1){
									log.debug("No of Addresses updated:"+arrLength);
								}
								
								
					
							});
							
							req.on('requestTimeout', function (req) {
								console.log('request has expired');
								req.abort();
							});

							req.on('responseTimeout', function (res) {
								console.log('response has expired');

							});

							//it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts
							req.on('error', function (err) {
								console.log('request error', err);
							});
				
				}
				
			}
			
		});

        
        
    });    
}
  

readUpdateCSVFile();

