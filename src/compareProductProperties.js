const fs = require("fs");
var createCSVFile = require('csv-file-creator')
const csv = require('csv-parser');

let product_edm_rows = {}
let product_rows = {}
let values = []
const log = require('simple-node-logger').createSimpleLogger('../log/product-edm-cxc-diff.log');
log.setLevel('all');



fs.createReadStream("C:../data/EDM_item_status.csv")
  .pipe(csv({ delimiter: ",", from_line: 2 }))
  .on("data", function (row) {
    product_edm_rows[row['id']] = row;

  })
  .on("end", function () {
    console.log("finished");

    fs.createReadStream("C:../data/CXC_item_status.csv")
      .pipe(csv({ delimiter: ",", from_line: 1, record_delimiter: "\n", }))
      .on("data", function (row) {
        product_rows[row['id']] = row
      })
      .on("end", function () {
        let product_table = []

        let headers = ["id", "x_itemStatus025", "x_itemStatus030", "x_itemStatus040", "x_itemStatus401", "x_itemStatus402", "x_itemStatus403", "x_itemStatus404", "x_itemStatus412", "x_itemStatus413", "x_itemStatus414", "x_itemStatus415", "x_itemStatus421", "x_itemStatus425", "x_itemStatus434", "x_itemStatus706", "x_itemStatus711", "x_itemStatus715", "x_itemStatus719", "x_itemStatus728", "x_itemStatus729", "x_itemStatus732", "x_itemStatus733", "x_itemStatus735", "x_itemStatus737", "x_itemStatus799"]
        product_table.push(headers)

        for (const [id, product_edm_row] of Object.entries(product_edm_rows)) {
          if (id in product_rows) {
            product_row = product_rows[id]
            for (const header of headers) {
              if (product_edm_row[header] !== product_row[header]) {
                log.debug(`id: ${id},   Warehouse Org: ${header},   EDM Value: ${product_edm_row[header]},   CXC Value: ${product_row[header]}`);
                values = Object.values(product_edm_row)
                product_table.push(values)
                break;
              }
            }
          }
        }
        createCSVFile("../output/File5.csv", product_table);
      })
      .on("error", function (error) {
        console.log(error.message);
      });

  })
  .on("error", function (error) {
    console.log(error.message);
  });


