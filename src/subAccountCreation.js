var fs = require('fs'),
         createCSVFile = require('csv-file-creator'),
         testExt= '../data/StageAccounts.json';
         DealerExt='../data/StageExtract.json';
         
         SubExport = JSON.parse(fs.readFileSync(testExt));
         PriExport = JSON.parse(fs.readFileSync(testExt));

         dealerExtract = JSON.parse(fs.readFileSync(DealerExt));
var PriData={};
var PriOrg=[];
var SubData={};
var SubOrg=[];


//const csv = require('csv-parser');
PriExport.organization.forEach(function(organization) {
	
			if(organization.contract!=null && organization.parentOrganization==null && organization.active==true && organization.tmsCustomerAccountNumber && organization.tmsCustomerAccountId){
				for (let i= 0; i < dealerExtract.length; i++) {
					
					
						if (organization.name == dealerExtract[i].name && organization.id == dealerExtract[i].id) {
							
							  
							organization.siteOrganizationProperties.forEach(element => {
								if (element.properties.contract!=null) {
									element.properties.contract=null;
									//console.log(organization.name);

									
								}
								
							});
							
							//console.log(organization.ancestorOrganizations);
							if(organization.derivedContract && organization.derivedContract.priceListGroup && organization.derivedContract.priceListGroup.id){
								
								organization.derivedContract = null;
							}
							
							if(organization.contract && organization.contract.priceListGroup && organization.contract.priceListGroup.id){
								
								organization.contract= null;
							}

							organization.brandShortName="Multi Brand";
							PriOrg.push(organization);
							PriData.organization=PriOrg;
							
						} 
					
					
						   
				} 
			}
	
});
    
SubExport.organization.forEach(function(organization) {
	
		if(organization.contract!=null && organization.parentOrganization==null && organization.active==true && organization.tmsCustomerAccountNumber && organization.tmsCustomerAccountId){
			for (let i= 0; i < dealerExtract.length; i++) {
				
				
					if (organization.name == dealerExtract[i].name && organization.id == dealerExtract[i].id) {
						
						// for (let index = 0; index < organization.members.length; index++) {
						//     var element = organization.members[index];
						//     userData.push([element.id]);
						//     console.log(userData);
						// }
						var lastIndex = organization.name.split(" ").splice(-1);
						var accountName= organization.name.lastIndexOf(" ");
						
						var brandName=dealerExtract[i].brandShortName.split(" ").splice(-1);
						
						if(lastIndex[0]==dealerExtract[i].brandShortName || lastIndex[0]==brandName ){

						organization.name= organization.name +" .";
						}else{
						organization.name+=" "+dealerExtract[i].brandShortName;
						}
						console.log(organization.name);
						
						
							if(organization.brandShortName=="Utilities"){
								organization.siteOrganizationProperties.forEach(element => {
									if (element.properties.contract!=null) {
										//console.log(organization.name);
										element.properties.useAllPaymentMethodsFromSite=false;
										element.properties.paymentMethods=[];
										
										delete element.properties.contract.repositoryId;
										delete element.properties.contract.creationDate;

										
									}
							
								});
								organization.paymentMethods=[];
							}else{
								organization.siteOrganizationProperties.forEach(element => {
									if (element.properties.contract!=null) {
										//console.log(organization.name);
										element.properties.useAllPaymentMethodsFromSite=false;
										
										
										delete element.properties.contract.repositoryId;
										delete element.properties.contract.creationDate;

										
									}
							
								});
								
								organization.paymentMethods=["invoice"];
							}
						organization.useAllPaymentMethodsFromSite=false;
							
						
						
						
						var data = {};
						data.repositoryId = organization.id;
						data.name = dealerExtract[i].DealerName;
						data.active =organization.active;
						data.id = organization.id;
						
						organization.parentOrganization = "";

						organization.parentOrganization = data;
						//console.log(organization.parentOrganization);

						organization.ancestorOrganizations.push(data);
						organization.secondaryAddresses = [];
						organization.shippingAddress = null;
						organization.billingAddress = null;
						organization.members=[];
						
						//console.log(organization.ancestorOrganizations);
						if(organization.derivedContract && organization.derivedContract.priceListGroup && organization.derivedContract.priceListGroup.id){
							
							delete organization.derivedContract.repositoryId;
							delete organization.derivedContract.creationDate;
						}
						

						if(organization.contract && organization.contract.priceListGroup && organization.contract.priceListGroup.id){
							
							delete organization.contract.repositoryId;
							delete organization.contract.creationDate;
						}
						
						
						delete organization.id;
						SubOrg.push(organization);
						SubData.organization=SubOrg;
						
					} 

				
					   
			}
		}
	
});
         
 

fs.writeFileSync('../output/DeletePrimaryContract.json',JSON.stringify(PriData));
fs.writeFileSync('../output/SubAccounts.json', JSON.stringify(SubData));

