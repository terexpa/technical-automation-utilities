const express = require("express");
const xlsx = require("xlsx");
const path = require("path");

//Before running the scirpt paste the file in data which need to convert
//change FILE_NAME variable without extension.
//after generated file, main cxc head collection id should be
// combination of "dws_" and cxc head collection of that brand catalog, like for mp powescreen,
// cxc id is mpPowerscreen, docwre strture main collection id is dws_mpPowerscreen

const FILE_NAME = "MPS_hosur1"

const workbook = xlsx.readFile(path.normalize(path.join(__dirname, "..", "data", `${FILE_NAME}.xlsx`)));
const JsonArray = workbook.SheetNames.map((sheetName) => {
  const worksheet = workbook.Sheets[sheetName];
  const sheetData = xlsx.utils.sheet_to_json(worksheet);

  return sheetData;
});

const getCXCID = (item, arr, dataArray, Obj) => {
  const assemblyNumber = item["Assembly number"];
  const materialNumber = Obj["machine_id"];
  let replacedMaterialNumber = materialNumber.replace(/[^a-zA-Z0-9_-]/g, "--");
  replacedMaterialNumber = replacedMaterialNumber.replace(/--$/g, "");
  let id = "";
  const value = dataArray.find(
    (dataItem) => dataItem.machine_id == assemblyNumber
  );
  if (
    Object.keys(value) &&
    Object.keys(value).length &&
    Object.keys(value).length > 0
  ) {
    if (value.ID.includes("dws_")) {
      id = `pf_${replacedMaterialNumber}`;
    } else if (value.ID.includes("pf_")) {
      id = `pl_${replacedMaterialNumber}`;
    } else if (value.ID.includes("pl_")) {
      id = `md_${replacedMaterialNumber}`;
    } else if (value.ID.includes("md_")) {
      id = `sl_${replacedMaterialNumber}`;
    }
  }
  const newObj = {
    ...value,
    fixedChildCategories:
      value["fixedChildCategories"] == ""
        ? id
        : `${value["fixedChildCategories"]},${id}`,
  };
  const dataIndex = dataArray.findIndex((dataItem) => dataItem.ID === value.ID);
  if (dataIndex != -1) {
    dataArray[dataIndex] = newObj;
  }

  return id;
};

const csvData = JsonArray[0].reduce((dataArray, item, index, arr) => {
  const Obj = {};
  Obj["ID"] = "";
  Obj["name"] = item["Machine Category/Name"];
  Obj["displayName"] = item["Machine Category/Name"];
  Obj["description"] = "";
  Obj["longDescription"] = "";
  Obj["myActive"] = true;
  Obj["fixedChildProducts"] = "";
  Obj["fixedChildCategories"] = "";
  Obj["seoDescription"] = "";
  Obj["seoKeywords"] = "";
  Obj["seoTitle"] = "";
  Obj["seoUrlSlug"] = "";
  Obj["images"] = "";
  Obj["altText"] = "";
  Obj["titleText"] = "";
  Obj["excludeFromSitemap"] = "";
  Obj["machine_id"] = item["Material number"];
  if (index === 0) {
    Obj["ID"] = `dws_${item["Material number"]}`;
    Obj["name"] = `Non-Navigable ${item["Machine Category/Name"]}`;
    Obj["displayName"] = `Non-Navigable ${item["Machine Category/Name"]}`;
    dataArray.push(Obj);
    return dataArray;
  }
  Obj["ID"] = getCXCID(item, arr, dataArray, Obj);
  dataArray.push(Obj);
  return dataArray;
}, []);
const prefixes = ["sl", "md", "pl", "pf", "dws"];
const getSortedValue = (item) => {
  const prefixedValue = item.ID.split("_")[0];
  return prefixes.indexOf(prefixedValue);
};

const sortedArray = csvData.sort(
  (item1, item2) => getSortedValue(item1) - getSortedValue(item2)
);
const header1 = [
  "/atg/commerce/catalog/ProductCatalog:category",
  ,
  ,
  ,
  "LOCALE=en",
  "CATALOG=cloudCatalog",
];
const header2 = [
  "ID",
  "name",
  "displayName",
  "description",
  "longDescription",
  "myActive",
  "fixedChildProducts",
  "fixedChildCategories",
  "seoDescription",
  "seoKeywords",
  "seoTitle",
  "seoUrlSlug",
  "images",
  "altText",
  "titleText",
  "excludeFromSitemap",
  "machine_id",
];
let wb = xlsx.utils.book_new();
const dataArrayValues = sortedArray.map((item) => Object.values(item));
const csvDataArray = [header1, header2, ...dataArrayValues];
let ws = xlsx.utils.aoa_to_sheet(csvDataArray);
xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
xlsx.writeFile(wb, path.normalize(path.join("..","output",`${FILE_NAME}.csv`)));
