let fs = require('fs');
         fileName = '../dist/ProdComparison.json';
         allAccount = JSON.parse(fs.readFileSync(fileName));
const csv = require('csv-parser');
//let tmsCustomerAccountNumberCollection = {};
let custMapForAddress = {};    
//let noTMSCustomerID = [];

fs.createReadStream('../dist/ProdCustomerExtract.csv')
.pipe(csv())
.on('data', function(data){
    try {
         
        let addressData = {};
        if(data.CUSTACCTBILLTOFLAG == "P" || data.CUSTACCTSHIPTOFLAG == "P"){
            //console.log(data);
            if(data.CUSTACCTBILLTOFLAG == "P" && data.CUSTACCTSHIPTOFLAG == "P") {
                
                addressData = {
                    isDefaultBillingAddress : true,
                    isDefaultShippingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }
                //console.log('if');
            }else if (data.CUSTACCTSHIPTOFLAG == "P"){
                addressData = {
                    isDefaultShippingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }
                
               // console.log('1 elseif');
            }else if(data.CUSTACCTBILLTOFLAG == "P") {
                addressData = {
                    isDefaultBillingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }

                //console.log('2 elseif');
            }

            if(custMapForAddress[data['CUSTOMERACCOUNTNUMBER']] && custMapForAddress[data['CUSTOMERACCOUNTNUMBER']].length) {
                  custMapForAddress[data['CUSTOMERACCOUNTNUMBER']].push(addressData);
            }else {
                custMapForAddress[data['CUSTOMERACCOUNTNUMBER']] = [addressData];
            }
        }
            

    }
    catch(err) {
        //console.log(err)
    }
})
.on('end',function(){
    console.log(custMapForAddress);
    fs.writeFileSync('custMapForAddress.json', JSON.stringify(custMapForAddress));
    createPrinciaplAccounts();
});


function createPrinciaplAccounts() {
    let orgObj = {'organization':[]};
        for (var i = 0, len = allAccount.length; i < len; i++) {
            let data={};
            data.organizationLogoURL = "general/logo-utilities_Color.png";
                data.derivedOrganizationLogo = data.organizationLogoURL;
                data.organizationLogo = data.organizationLogoURL;
                data.relativeRoles = [{
					"relativeTo": null,
					"function": "admin",
					"type": "organizationalRole"
				}, {
					"relativeTo": null,
					"function": "buyer",
					"type": "organizationalRole"
				}, {
					"relativeTo": null,
					"function": "approver",
					"type": "organizationalRole"
				}, {
					"relativeTo": null,
					"function": "accountAddressManager",
					"type": "organizationalRole"
				}, {
					"relativeTo": null,
					"function": "profileAddressManager",
					"type": "organizationalRole"
				}
			];
                data.parentOrganization = null;
                //data.members = organization.members;
                data.members =[];
              
                data.contract = {
                    "catalog": {
                        "id": "025_TerexUtilities"
                    },
                    "displayName": "Terex Utilities",
                    "priceListGroup": {
                        "id": "USD_UTILITIES"
                    }
                };

                data.siteOrganizationProperties = [  
                {  
                   "site":{  
                      "siteId":"siteUS"
                   },
                   "properties":{  
                      "useAllShippingMethodsFromSite":true,
                      "useAllPaymentMethodsFromSite": false,
                      "approvalRequired":false,
                      "contract": {
                          "catalog":{
                              "id": "025_TerexUtilities"
                          },
                          "displayName":"Terex Utilities",
                          "priceListGroup": {
                             "id":"USD_UTILITIES"
 
                          }
                      },
                      "paymentMethods":[],
                      "delegateApprovalManagement":false,
                      "useExternalApprovalWebhook":false,
                      "orderPriceLimit":null,
                      "shippingMethods":[  
          
                      ]
                   }
                }
             ];
                data.paymentMethods=[];
                data.shippingMethods=[];
                data.derivedContract = {
                    "catalog": {
                        "id": "025_TerexUtilities"
                    },
                    "displayName": "Terex Utilities",
                    "priceListGroup": {
                        "id": "USD_UTILITIES"
                    }
                };
                data.useAllPaymentMethodsFromSite=true;
                data.useAllShippingMethodsFromSite=false;

                //data.name= allAccount[i].CUSTOMERNAME;
                data.active = true;
                data.brandShortName = "Utilities";
                data._usDefaultCatalog = true,
                data.terex_secondaryitemwarehouselocationid = "21415",
                data.warehouseOrganizationCode = "728";
                data.tmsCustomerAccountId = allAccount[i].CUSTOMERACCOUNTID;
                data.tmsCustomerAccountNumber = allAccount[i].CUSTOMERACCOUNTNUMBER;
                data.warehouseLocationId = "16117";
        
                let addresses = custMapForAddress[allAccount[i].CUSTOMERACCOUNTNUMBER];
                //console.log(addresses);
                data.allSecondaryAddresses = {};
                // if(!organization.tmsCustomerAccountId){
                //     noTMSCustomerID.push({"name":organization.name, "id": organization.id});
                //     // console.log('organization.tmsCustomerAccNum .......',organization.tmsCustomerAccountNumber);
                // //console.log('Org id', organization.name);
                // }
               
                addresses.forEach(function(address){
                    data.allSecondaryAddresses[address.tms_customerAccountSiteId] = address;
                    console.log(data.allSecondaryAddresses);
                    
                    if(address.isDefaultBillingAddress) {
                        data.billingAddress = address;
                        data.derivedBillingAddress = address;
                    }
                    if(address.isDefaultShippingAddress) {
                        data.shippingAddress = address;
                        data.derivedShippingAddress = address;
                    }
                });
				
				data.name= allAccount[i].CUSTOMERNAME;
				
                data.dealerContactUs = "<p style=\"margin-top:20px; margin-bottom:10px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN\" style=\"font-size:22.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">Parts &ndash; Customer Service<\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN\" style=\"font-size:13.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">For assistance with Parts Ordering, Order Status, Identification, Pricing, or other inquiries related to replacement parts please reach out to our Parts Customer Service team:<\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><span lang=\"EN\" style=\"font-size:13.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">Email: <\/span><\/span><\/b><span lang=\"EN\" style=\"font-size:13.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">&nbsp;<span class=\"MsoHyperlink\" style=\"color:blue\"><span style=\"text-decoration:underline\"><b><span style=\"color:#d3171f\"><a href=\"mailto:Utilities.Parts@terex.com\" style=\"color:blue; text-decoration:underline\"><span style=\"color:#d3171f\">Utilities.Parts@terex.com<\/span><\/a><\/span><\/b><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\">&nbsp;<\/p>\n\n<p style=\"margin-top:20px; margin-bottom:10px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN\" style=\"font-size:22.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">Technical Service &amp; Support<\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">Our first-class Service Network is available Monday-Friday to answer your questions and get you pointed in the right direction. Each member of the service department has years of experience with our products and access to all the available information on our units.<\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">Our technical support team is available by emailing the&nbsp;<\/span><\/span><\/span><\/span><strong><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\">serial number<\/span><\/span><\/span><\/span><\/strong><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\">,<strong><span style=\"font-family:&quot;Roboto Condensed&quot;\">&nbsp;contact information<\/span><\/strong>,<\/span><\/span><\/span><\/span><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\"> and a&nbsp;<\/span><\/span><\/span><\/span><strong><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\">description of the problem&nbsp;<\/span><\/span><\/span><\/span><\/strong><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">to the address below. <\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><b><span lang=\"EN\" style=\"font-size:13.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">Email: <\/span><\/span><\/b><span lang=\"EN\" style=\"font-size:13.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">&nbsp;<span class=\"MsoHyperlink\" style=\"color:blue\"><span style=\"text-decoration:underline\"><b><span style=\"color:#d3171f\"><a href=\"mailto:Utilities.Service@terex.com\" style=\"color:blue; text-decoration:underline\"><span style=\"color:#d3171f\">Utilities.Service@terex.com<\/span><\/a><\/span><\/b><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p style=\"margin-bottom:16px\">&nbsp;<\/p>\n\n<p style=\"margin-top:20px; margin-bottom:10px\"><span style=\"font-size:11pt\"><span style=\"background:white\"><span style=\"line-height:normal\"><span style=\"font-family:Calibri,sans-serif\"><span lang=\"EN\" style=\"font-size:22.5pt\"><span style=\"font-family:&quot;Roboto Condensed&quot;\">Phone Support<\/span><\/span><\/span><\/span><\/span><\/span><\/p>\n\n<p><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">You may reach any of our Terex departments by phone by calling&nbsp;<\/span><\/span><\/span><\/span><\/span><strong><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\">1-844-Terex-4U (1-844-837-3948)<\/span><\/span><\/span><\/span><\/span><\/strong>&nbsp;<span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">and asking to speak to a member of Parts or Service. Please have the&nbsp;<\/span><\/span><\/span><\/span><\/span><b><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\">model &amp;<\/span><\/span><\/span><\/span><\/span><\/b><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#d3171f\"> <strong><span style=\"font-family:&quot;Roboto Condensed&quot;\">serial number&nbsp;<\/span><\/strong><\/span><\/span><\/span><\/span><\/span><span style=\"font-size:13.5pt\"><span style=\"background:white\"><span style=\"line-height:115%\"><span style=\"font-family:&quot;Roboto Condensed&quot;\"><span style=\"color:#333333\">of the unit available before calling to assist in routing your call to the correct support team.<\/span><\/span><\/span><\/span><\/span><\/p>";
                
                data.secondaryAddresses =  data.allSecondaryAddresses;
                
                //console.log(data);
                orgObj.organization.push(data);

            //console.log(allAccount[i].CUSTOMERNAME);
            
        }
        fs.writeFileSync('UtilitiesProdAccounts.json', JSON.stringify(orgObj));
        console.log('finished processing the file');

}
