var fs = require('fs'),
         createCSVFile = require('csv-file-creator'),
         fileName = '../data/Account.json';
         allAcc = JSON.parse(fs.readFileSync(fileName));
const csv = require('csv-parser');
var occRepArr = [['repositoryId','occAccountId','occAccountName','occBrand','active','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state']];
var tmsRepArr = []; 
var ouMapping = new Map([['025', 'Terex USA, LLC'], 
['401', 'Terex India  Private Limited'],['402', 'Terex India  Private Limited'], ['735', 'Terex  Global GmbH'], ['719', 'Terex  Global GmbH'],['728', 'Terex  Global GmbH'], ['706', 'Terex  Global GmbH'], ['729', 'Terex  Global GmbH'],['733', 'Terex  Global GmbH']]);   

allAcc.organization.forEach(function(organization){
    try{
            let billingData={};
			let data={}
			
    // if (organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId==organization.shippingAddress.repositoryId ){
		// billingData=organization.billingAddress;
		// billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		// billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		// billingData.brandShortName=organization.brandShortName;
		// billingData.customerName=organization.name;
		// billingData.occAccountId=organization.id;
		// billingData.isDefaultBillto="P";
		// billingData.isDefaultShipto="P";
		// console.log("billTO");
		
		
		// accData.push([billingData.repositoryId,organization.id,organization.name,organization.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
		
    // }else if(organization.billingAddress && organization.shippingAddress && organization.billingAddress.repositoryId!=organization.shippingAddress.repositoryId ){
		// console.log("shipTO");
		// data=organization.shippingAddress;
		// data.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		// data.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		// data.brandShortName=organization.brandShortName;
		// data.customerName=organization.name;
		// data.occAccountId=organization.id;
		// data.isDefaultBillto="";
		// data.isDefaultShipto="P";
		
		// billingData=organization.billingAddress;
		// billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		// billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		// billingData.brandShortName=organization.brandShortName;
		// billingData.customerName=organization.name;
		// billingData.occAccountId=organization.id;
		// billingData.isDefaultBillto="P";
		// billingData.isDefaultShipto="";
		
		// accData.push([data.repositoryId,organization.id,organization.name,organization.brandShortName,data.tmsCustomerAccountId,data.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,data.tms_customerAccountSiteId,data.postalCode,data.city,data.country,data.companyName,data.state,data.address3,data.address2,data.address1,data.county,data.tms_state,data.isDefaultBillto,data.isDefaultShipto]);
		// accData.push([billingData.repositoryId,organization.id,organization.name,organization.brandShortName,billingData.tmsCustomerAccountId,billingData.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,billingData.tms_customerAccountSiteId,billingData.postalCode,billingData.city,billingData.country,billingData.companyName,billingData.state,billingData.address3,billingData.address2,billingData.address1,billingData.county,billingData.tms_state,billingData.isDefaultBillto,billingData.isDefaultShipto]);
		
	// }else if(!organization.billingAddress){
		// billingData.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		// billingData.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		// billingData.brandShortName=organization.brandShortName;
		// billingData.customerName=organization.name;
		// billingData.occAccountId=organization.id;
		// billingData.isDefaultBillto="";
		// billingData.isDefaultShipto="";
		// accData.push(["",organization.id,organization.name,organization.brandShortName,"","",organization.warehouseOrganizationCode,organization.warehouseLocationId,"","","","","","","","","","","",billingData.isDefaultBillto,billingData.isDefaultShipto]);
	
	// }else if(!organization.shippingAddress){
		// data.tmsCustomerAccountId=organization.tmsCustomerAccountId;
		// data.tmsCustomerAccountNumber=organization.tmsCustomerAccountNumber;
		// data.brandShortName=organization.brandShortName;
		// data.customerName=organization.name;
		// data.occAccountId=organization.id;
		// data.isDefaultBillto="";
		// data.isDefaultShipto="";
		
		// accData.push(["",organization.id,organization.name,organization.brandShortName,"","",organization.warehouseOrganizationCode,organization.warehouseLocationId,"","","","","","","","","","","",data.isDefaultBillto,data.isDefaultShipto]);
	// }
		
	
	
			const allSecondaryAddresses = organization.allSecondaryAddresses;
                for (var key in allSecondaryAddresses) { 
                    if(allSecondaryAddresses.hasOwnProperty(key)) {
						

                        let data ={};
                        data.address=allSecondaryAddresses[key];
						
						
                        occRepArr.push([data.address.repositoryId,organization.id,organization.name,organization.brandShortName,organization.active,organization.tmsCustomerAccountId,organization.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,data.address.tms_customerAccountSiteId,data.address.postalCode,data.address.city,data.address.country,data.address.companyName,data.address.phoneNumber,data.address.state,data.address.address3,data.address.address2,data.address.address1,data.address.county,data.address.tms_state]);
						
                    }
                }
            
        }catch(err){
            console.log(organization.id);
            console.log(err);

        }
    

});
fs.createReadStream('../output/UpdateSiteId.csv')
.pipe(csv())
.on('data', function(data){
    try {
        //console.log('repository id:'+data.OCC_REPOSITORY_ID);
        tmsRepArr.push([data]);
        
    }
    catch(err) {
        //error handler
    }
})
.on('end',function(){
    //some final operation
    getMissingRepoId(occRepArr, tmsRepArr);
	
	
	
}); 

var getMissingRepoId = (occRepArr, tmsRepArr) => {
     console.log(occRepArr.length, tmsRepArr.length);
     let matchingRepoIds = [['repositoryId','occAccountId','occAccountName','occBrand','active','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
	 let missingRepoIds = [['CUSTOMERACCOUNTNUMBER','TMSCUSTOMERACCOUNTID','CUSTOMERACCOUNTSITEID','OCCREPOSITORYID','OCCACCOUNTID','CUSTOMERNAME','OCCBRAND','CUSTACCTBILLTOFLAG','CUSTACCTSHIPTOFLAG']];
        
     for(let j = 1;  j < tmsRepArr.length; j++) {
         let isMatchFound = false;
		 //let warehouseOrganizationCode = occRepArr[i][7];
    
        for(let i = 1; i < occRepArr.length; i++) {
			
             if(occRepArr[i][5]==tmsRepArr[j][0].TMSCUSTOMERACCOUNTID && occRepArr[i][6]==tmsRepArr[j][0].CUSTOMERACCOUNTNUMBER && occRepArr[i][9]== tmsRepArr[j][0].CUSTOMERACCOUNTSITEID){
                isMatchFound = true;
				
				matchingRepoIds.push([occRepArr[i][0],occRepArr[i][1],occRepArr[i][2],occRepArr[i][3],occRepArr[i][4],occRepArr[i][5],
				occRepArr[i][6],occRepArr[i][7],occRepArr[i][8],occRepArr[i][9],occRepArr[i][10],occRepArr[i][11],occRepArr[i][12],
				occRepArr[i][13],occRepArr[i][14],occRepArr[i][15],occRepArr[i][16],occRepArr[i][17],occRepArr[i][18],occRepArr[i][19],
				occRepArr[i][20],tmsRepArr[j][0].CUSTACCTBILLTOFLAG,tmsRepArr[j][0].CUSTACCTSHIPTOFLAG]);
                 break;
             }
        }
        if(!isMatchFound) {
			console.log("notfound");
             missingRepoIds.push([tmsRepArr[j][0].CUSTOMERACCOUNTNUMBER,tmsRepArr[j][0].TMSCUSTOMERACCOUNTID,tmsRepArr[j][0].CUSTOMERACCOUNTSITEID,tmsRepArr[j][0].OCCREPOSITORYID,tmsRepArr[j][0].OCCACCOUNTID,tmsRepArr[j][0].OCCBRAND,tmsRepArr[j][0].CUSTOMERNAME,tmsRepArr[j][0].CUSTACCTBILLTOFLAG,tmsRepArr[j][0].CUSTACCTSHIPTOFLAG]);
        } 
     }

     //console.log(missingRepoIds);
	 createCSVFile("../output/UpdateAddressMatchProd1.csv",matchingRepoIds);
	 createCSVFile("../output/UpdateAddressMissMatchProd1.csv",missingRepoIds);
	 console.log("file generated successfully");
	 return missingRepoIds;
	
 }




 



