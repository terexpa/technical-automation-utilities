var fs = require('fs'),
         createCSVFile = require('csv-file-creator'),
         fileName = '../data/AccountsV2.json';
         allAcc = JSON.parse(fs.readFileSync(fileName));
const csv = require('csv-parser');
var subRepArr = [['repositoryId','occAccountId','occAccountName','occBrand','active','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state']];
var priRepArr = []; 
//var ouMapping = new Map([['025', 'Terex USA, LLC'], 
//['401', 'Terex India  Private Limited'],['402', 'Terex India  Private Limited'], ['735', 'Terex  Global GmbH'], ['719', 'Terex  Global GmbH'],['728', 'Terex  Global GmbH'], ['706', 'Terex  Global GmbH'], ['729', 'Terex  Global GmbH'],['733', 'Terex  Global GmbH']]);   

allAcc.organization.forEach(function(organization){
    try{
            
			const allSecondaryAddresses = organization.allSecondaryAddresses;
                for (var key in allSecondaryAddresses) { 
                    if(allSecondaryAddresses.hasOwnProperty(key)) {
						

                        let data ={};
                        data.address=allSecondaryAddresses[key];
						
						
                        subRepArr.push([data.address.repositoryId,organization.id,organization.name,organization.brandShortName,organization.active,organization.tmsCustomerAccountId,organization.tmsCustomerAccountNumber,organization.warehouseOrganizationCode,organization.warehouseLocationId,data.address.tms_customerAccountSiteId,data.address.postalCode,data.address.city,data.address.country,data.address.companyName,data.address.phoneNumber,data.address.state,data.address.address3,data.address.address2,data.address.address1,data.address.county,data.address.tms_state]);
						
                    }
                }
	
	
			
                       
						
						
            
        }catch(err){
            console.log(organization.id);
            console.log(err);

        }
    

});
fs.createReadStream('../output/BrandNameAppended.csv')
.pipe(csv())
.on('data', function(data){
    try {
        //console.log('repository id:'+data.OCC_REPOSITORY_ID);
		
        priRepArr.push([data]);
        
    }
    catch(err) {
        //error handler
    }
})
.on('end',function(){
    //some final operation
    getMissingRepoId(priRepArr, subRepArr);
	
	
	
}); 

var getMissingRepoId = (priRepArr, subRepArr) => {
     console.log(priRepArr.length, subRepArr.length);
	 let principalData=[['id','name','active','brandShortName','tmsCustomerAccountNumber','tmsCustomerAccountId','parentOrganization','contract']];
	 
     let matchingRepoIds = [['repositoryId','occAccountId','occAccountName','occBrand','active','tmsCustomerAccountId','tmsCustomerAccountNumber','warehouseOrganizationCode','warehouseLocationId','tms_customerAccountSiteId','postalCode','city','country','companyName','phoneNumber','state','address3','address2','address1','county','tms_state']];
	 
        
     for(let i = 1;  i < priRepArr.length; i++) {
         let isMatchFound = false;
		 
		let accountName=priRepArr[i][0].name;
		let brandName=priRepArr[i][0].brandShortName;
		var lastIndex = priRepArr[i][0].name.split(" ").splice(-1);
						
						
		var brand=priRepArr[i][0].brandShortName.split(" ").splice(-1);
		if(lastIndex[0]==brand){
			console.log(priRepArr[i][0].name);
		//principalData.push([priRepArr[i][0].id,priRepArr[i][0].name,priRepArr[i][0].active,priRepArr[i][0].brandShortName,priRepArr[i][0].tmsCustomerAccountNumber,priRepArr[i][0].tmsCustomerAccountId,priRepArr[i][0].parentOrganization,priRepArr[i][0].contract]);
		var name=accountName.replace(brandName,"");
		priRepArr[i][0].name=name.replace(/\s*$/, '');
		console.log(priRepArr[i][0].name);
		
		}
		 
    
        for(let j = 1; j < subRepArr.length; j++) {
			
             if(priRepArr[i][0].name==subRepArr[j][2]){
                isMatchFound = true;
				
				matchingRepoIds.push([subRepArr[j][0],subRepArr[j][1],subRepArr[j][2],subRepArr[j][3],subRepArr[j][4],subRepArr[j][5],
				subRepArr[j][6],subRepArr[j][7],subRepArr[j][8],subRepArr[j][9],subRepArr[j][10],subRepArr[j][11],subRepArr[j][12],
				subRepArr[j][13],subRepArr[j][14],subRepArr[j][15],subRepArr[j][16],subRepArr[j][17],subRepArr[j][18],subRepArr[j][19],
				subRepArr[j][20]]);
                 
             }
        }
        
     }

     //console.log(missingRepoIds);
	 createCSVFile("../output/MatchingAppendedAddresses.csv",matchingRepoIds);
	 //createCSVFile("../output/BrandNameAppendedAddresses.csv",matchingRepoIds);
	// createCSVFile("../output/BrandNameAppended.csv",principalData);
	 
	 console.log("file generated successfully");
	 return matchingRepoIds;
	
 }




 



