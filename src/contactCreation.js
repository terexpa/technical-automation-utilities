const Client = require('node-rest-client').Client;
const fs = require('fs');
//Needed to parse response when mimeTypes is not set :util.inspect(object)
//var util = require('util');
const csv = require('csv-parser');
let profileIds = [];
//Setup logs
const log = require('simple-node-logger').createSimpleLogger('../log/contact-update1.log');
log.setLevel('all');

// customize response to avoid circular structure
 var options = { 
    mimetypes:{
        json:["application/json","application/json; charset=utf-8"]
    }  
 };
 
var client = new Client(options);

function createRequestdata(data) {
    let reqData = {
        firstName: data.FirstName,
		lastName: data.LastName,
		email: data.EmailId,
		receiveEmail: "yes",
		profileType: "b2b_user",
		active: true
        
    };
    return reqData;
}


function readContactCSVFile() {
    fs.createReadStream('../output/finalusercsv.csv')
    .pipe(csv())
    .on('data', function(data){
        try {
            //  createRequestdata(data);
            profileIds.push(data);
        }
        catch(err) {
            log.error('Error while reading CSV', err);
        }
    })
    .on('end',function(){
		var adminAuth='';
		var loginReqData =	{
			grant_type: "password",
			username: "unnati.barot@lntinfotech.com",
			password: "Terex@123",
			totp_code: "158456"
		};
		let args={
				data:loginReqData,
				headers:{"Content-Type": "application/x-www-form-urlencoded"}
		};
		client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/login", args, function(data, response){
			console.log("Response"  +JSON.parse(data));
			console.log("status" +response.statusCode);
			adminAuth=JSON.parse(data);
			console.log("accessToken:" +adminAuth.access_token);
			if(adminAuth.access_token!=""){
				
				profileIds.forEach((data) => {
					if(data.IsNew=="Yes"){
							let reqData = createRequestdata(data);
							var auth_token = adminAuth.access_token;
							let args = {
								data: reqData,
							   // path:{"id":data.id},
								headers: { "Content-Type": "application/json","Authorization":'Bearer'+' '+auth_token}
							};
							
							log.debug("Request Data for contact:"+ args.data.email, reqData)
							process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

							client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/profiles", args, function(data, response) {
								
								console.log(JSON.parse(data));
								
								log.debug("Response for contact:"+args.data.email +" status "+response.statusCode);
					
							});
					}
				});
			}
			
		});

        
        
    });    
}
  

readContactCSVFile();

