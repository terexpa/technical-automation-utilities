const Client = require('node-rest-client').Client;
const fs = require('fs');
//Needed to parse response when mimeTypes is not set :util.inspect(object)
//var util = require('util');
const csv = require('csv-parser');
let accountIds = [];
//Setup logs
const log = require('simple-node-logger').createSimpleLogger('../log/account-update-Prod1.log');
log.setLevel('all');

// customize response to avoid circular structure
 var options = { 
    mimetypes:{
        json:["application/json","application/json; charset=utf-8"]
    }  
 };
 
var client = new Client(options);

function createRequestdata(data) {
    let reqData = {
			"siteOrganizationProperties": [{
					"properties": {
						"contract": {
							"displayName": "025_Demag",
							"catalogId": "025_DemagCranes",
							"priceListGroupId": "defaultPriceGroup",
							"description": null,
							"externalContractReference": null,
							"contract_terms": {
								"terms": null
							}
						}
					},
					"site": {
						"id": "siteUS"
					}
				}
			]
	};
    return reqData;
}


function readAccountCSVFile() {
    fs.createReadStream('../data/DemagProd.csv')
    .pipe(csv())
    .on('data', function(data){
        try {
            //  createRequestdata(data);
            accountIds.push(data);
        }
        catch(err) {
            log.error('Error while reading CSV', err);
        }
    })
    .on('end',function(){
		var adminAuth='';
		var loginReqData =	{
			grant_type: "password",
			username: "unnati.barot@lntinfotech.com",
			password: "Pramukh100",
			totp_code: "784312"
		};
		let args={
				data:loginReqData,
				headers:{"Content-Type": "application/x-www-form-urlencoded"}
		};
		client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/login", args, function(data, response){
			//console.log("Response"  +JSON.parse(data));
			//console.log("status" +response.statusCode);
			adminAuth=JSON.parse(data);
			//console.log("accessToken:" +adminAuth.access_token);
			if(adminAuth.access_token!=""){
				var count =0;
				
				accountIds.forEach((data) => {
					
							let reqData = createRequestdata(data);
							var auth_token = adminAuth.access_token;
							let args = {
								data: reqData,
							    path:{"id":data.id},
								headers: { "Content-Type": "application/json","Authorization":'Bearer'+' '+auth_token}
							};
							
							log.debug("Request Data for account:"+ args.path.id, reqData)
							process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

							client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/organizations/${id}/updateSiteOrganizationProperties", args, function(data, response) {
								
								//console.log(JSON.parse(data));
								if(response.statusCode=='200'){
								count++;
								console.log(count);
								}
								
								log.debug("Response for account:"+args.path.id +" status "+response.statusCode);
								//log.debug("Total accounts updated:"+count);
					
							});
							
					
				});
			}
			
		});

        
        
    });    
}
  

readAccountCSVFile();

