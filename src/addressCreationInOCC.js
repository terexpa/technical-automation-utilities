const Client = require('node-rest-client').Client;
const fs = require('fs');
//Needed to parse response when mimeTypes is not set :util.inspect(object)
//var util = require('util');
const csv = require('csv-parser');
let addressIds = [];
//Setup logs
const log = require('simple-node-logger').createSimpleLogger('../log/address-update.log');
log.setLevel('all');

// customize response to avoid circular structure
 var options = { 
    mimetypes:{
        json:["application/json","application/json; charset=utf-8"]
    }  
 };
 
var client = new Client(options);

function createRequestdata(data) {
   
	let reqData = {};
	let addressData = {};
	
        if(data.CUSTACCTBILLTOFLAG == "P" || data.CUSTACCTSHIPTOFLAG == "P"){
            //console.log(data);
            if(data.CUSTACCTBILLTOFLAG == "P" && data.CUSTACCTSHIPTOFLAG == "P") {
                
                addressData = {
                    isDefaultBillingAddress : true,
                    isDefaultShippingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }
                //console.log('if');
            }else if (data.CUSTACCTSHIPTOFLAG == "P"){
                addressData = {
                    isDefaultShippingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }
                
               // console.log('1 elseif');
            }else if(data.CUSTACCTBILLTOFLAG == "P") {
                addressData = {
                    isDefaultBillingAddress : true,
                    address1: data.ADDRESS1,
                    address2: data.ADDRESS2,
                    address3: data.ADDRESS3,
                    tms_address4: data.ADDRESS4,
                    country: data.COUNTRY,
                    phoneNumber:data.PHONENUMBER,
                    city: data.CITY,
                    postalCode:data.POSTALCODE,
                    companyName: data.CUSTOMERNAME,
                    state: "OCC_NA",
                    county: data.COUNTY,
                    tms_customerAccountSiteId: data.CUSTOMERACCOUNTSITEID,
                    tms_state: data.STATE
                }

                //console.log('2 elseif');
            }
			reqData.address={
				"address":addressData,
				"addressType":data.CUSTOMERACCOUNTNUMBER
			}
			console.log(reqData);
    return reqData;
}


function readSiteCSVFile() {
    fs.createReadStream('../data/NoSiteIdInOCC.csv')
    .pipe(csv())
    .on('data', function(data){
        try {
            //  createRequestdata(data);
            addressIds.push(data);
        }
        catch(err) {
            log.error('Error while reading CSV', err);
        }
    })
    .on('end',function(){
		var adminAuth='';
		var loginReqData =	{
			grant_type: "password",
			username: "unnati.barot@lntinfotech.com",
			password: "System@123",
			totp_code: "158456"
		};
		let args={
				data:loginReqData,
				headers:{"Content-Type": "application/x-www-form-urlencoded"}
		};
		client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadminui/v1/login", args, function(data, response){
			console.log("Response"  +JSON.parse(data));
			console.log("status" +response.statusCode);
			adminAuth=JSON.parse(data);
			console.log("accessToken:" +adminAuth.access_token);
			if(adminAuth.access_token!=""){
				
				profileIds.forEach((data) => {
					
							let reqData = createRequestdata(data);
							var auth_token = adminAuth.access_token;
							let args = {
								data: reqData,
							    path:{"id":data.occAccountId},
								headers: { "Content-Type": "application/json","Authorization":'Bearer'+' '+auth_token}
							};
							
							log.debug("Request Data for org:"+ args.path.id, reqData)
							process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

							// client.post("https://ccadmin-prod-zb8a.oracleoutsourcing.com/ccadmin/v1/organizations/${id}/secondaryAddresses", args, function(data, response) {
								
								// console.log(JSON.parse(data));
								
								// log.debug("Response for Org:"+JSON.parse(data) +" status "+response.statusCode);
					
							// });
					
				});
			}
			
		});

        
        
    });    
}
  
readSiteCSVFile();

